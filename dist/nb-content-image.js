/*globals $:false*/
'use strict';

var React = require('react');
var PropertiesModal = require('nb-modal').PropertiesModal;

var nbContentImage = React.createClass({
  getInitialState: function () {
    return {
      img: undefined,
      show: false
    };
  },

  showModalProperties: function (e) {
    var state = this.state;
    var img = e.target;

    state.img = img;

    state.show = true;
    this.setState(state);
  },

  handleSave: function (img) {
    var state = this.state;
    var props = this.props;

    state.img = img;
    state.show = false;

    this.setState(state, function () {
      props.onChange(img);
    });
  },

  handleCancel: function () {
    var state = this.state;
    state.show = false;
    this.setState(state);
  },

  render: function () {
    var state = this.state;
    var props = this.props;

    var imgObj = {
      src: props.src,
      alt: props.alt,
      show: false,
      width: props.width || String(props.defaultWidth).concat('px'),
      height: props.height || String(props.defaultHeight).concat('px')
    };

    if (state.img) {
      var image = $(state.img);

      imgObj.src = image.attr('src');
      imgObj.alt = image.attr('alt');
      imgObj.width = image.css('width');
      imgObj.height = image.css('height');
    }

    return React.createElement(
      'div',
      null,
      React.createElement('img', {
        src: imgObj.src || 'https://placeholdit.imgix.net/~text?txtsize=33&txt=image&w=' + String(props.defaultWidth) + '&h=' + String(props.defaultHeight),
        alt: imgObj.alt || '',
        onClick: this.showModalProperties,
        className: props.className,
        style: {
          width: imgObj.width,
          height: imgObj.height
        }
      }),
      React.createElement(PropertiesModal, {
        img: this.state.img,
        show: this.state.show,
        onSave: this.handleSave,
        onCancel: this.handleCancel
      })
    );
  }
});

module.exports = nbContentImage;